<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/processed', 'PagesController@processed')->name('processed');
Route::get('/unprocessed', 'PagesController@unprocessed')->name('unprocessed');
Route::get('/upload', 'PagesController@upload')->name('upload');
Route::get('/data/delete/{id}', 'PagesController@deleteData')->name('deleteData');
Route::get('/profile', 'PagesController@profile')->name('profile');
Route::get('/view/unprocessed/{id}', 'PagesController@viewUnproccessed')->name('viewUnproccessed');
Route::get('/view/processed/{id}', 'PagesController@viewProccessed')->name('viewProccessed');
Route::get('/rocessed/delete/{id}', 'PagesController@deleteProcessed')->name('deleteProcessed');
Route::post('/search', 'PagesController@search')->name('search');

Route::post('/submitData', 'DataController@submitData')->name('submitData');
Route::post('/comment/{id}', 'DataController@submitComment')->name('submitComment');
Route::get('/getData', 'DataController@getData')->name('getData');
Route::post('/password', 'DataController@submitPassword')->name('submitPassword');
Route::post('/profile/submit', 'DataController@submitProfile')->name('submitProfile');

Route::get('/users', 'PagesController@users')->name('users');
Route::post('/users', 'DataController@adduser')->name('submituser');
Route::get('/user/{id}', 'PagesController@editUser')->name('editUser');
Route::post('/user/role/{id}', 'DataController@updateRole')->name('updateRole');
Route::get('/processed/delete/{id}', 'DataController@deleteProcessed')->name('deleteProcessed');

Route::get('inactive', 'PagesController@userLogout')->name('userLogout');

Route::get('/test', 'PagesController@test')->name('test');

//estates
Route::get('/estates', 'PagesController@estates')->name('estates');
Route::post('/estates', 'DataController@submitEstate')->name('submitEstate');
Route::get('/estates/data', 'DataController@estateData')->name('estateData');
Route::get('/estates/viewtask/{id}', 'PagesController@viewtask')->name('viewtask');
Route::post('/estates/add', 'PagesController@addestate')->name('addestate');
