<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estate extends Model
{
    protected  $fillable=['name','region','oltname','houses','occupacy','coordinates','job'];

    public  function task(){
        return $this->hasMany(Task::class);
    }
}
