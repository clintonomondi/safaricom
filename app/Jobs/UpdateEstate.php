<?php

namespace App\Jobs;

use App\Estate;
use App\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateEstate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $estates = Estate::all();
        foreach ($estates as $estate) {
            $id=$estate->id;
            $count = Task::where('status2', 'Pending')->where('summery', 'LIKE', '%' . $estate->name . '%')->update(['estate_id' => $id]);

        }
    }
}
