<?php

namespace App\Imports;

use App\Task;
use Maatwebsite\Excel\Concerns\ToModel;

class TasksImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Task([
            'incidentId'=>$row[0] ,
            'lastName'=>$row[1] ,
            'firstName'=>$row[2] ,
            'summery'=>$row[3] ,
            'service'=>$row[4] ,
            'status'=>$row[5] ,
            'group'=>$row[6] ,
            'reportedDate'=>$row[7] ,
            'targetDate'=>$row[8] ,
            'SLMRealTimeS'=>$row[9] ,
            'duration'=>$row[10] ,
            'resolution'=>$row[11] ,
            'note'=>$row[12] ,
        ]);
    }
}
