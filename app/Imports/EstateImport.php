<?php

namespace App\Imports;

use App\Estate;
use Maatwebsite\Excel\Concerns\ToModel;

class EstateImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Estate([
            'name'=>$row[0] ,
            'region'=>$row[1] ,
            'oltname'=>$row[2] ,
            'houses'=>$row[3] ,
            'occupacy'=>$row[4] ,
            'coordinates'=>$row[5] ,
        ]);
    }
}
