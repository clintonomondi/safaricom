<?php

namespace App\Http\Controllers;

use App\Estate;
use App\Task;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public  function unprocessed(){
        $tasks=Task::orderBy('id','asc')->where('status2','Pending')->get();
        return view('unprocessed',compact('tasks'));
    }

    public  function processed(){
        $tasks=Task::orderBy('id','desc')->where('status2','Done')->get();
        return view('processed',compact('tasks'));
    }

    public  function upload(){
        return view('upload');
    }

    public  function estates(){
        return view('uploadestate');
    }

    public  function deleteData($id){
        $data=Task::findorFail($id);
        $data->delete();
      return  redirect()->back()->with('success','Data deleted successfully');
    }

    public  function profile(){
        return view('profile');
    }

    public  function viewUnproccessed($id){
        $data=Task::findorFail($id);
        return view('viewunprocessed',compact('data'));
    }

    public  function deleteProcessed($id){
        $data=Task::findOrFail($id);
        $data->delete();
        return redirect()->back()->with('success','Data deleted successfully');
    }

    public  function search(Request $request){
        $q = $request->input('q');
        $tasks = Task::where('incidentId', 'LIKE', '%' . $q . '%')->orWhere('lastName', 'LIKE', '%' . $q . '%')->orWhere('firstName', 'LIKE', '%' . $q . '%')->orWhere('summery', 'LIKE', '%' . $q . '%')->orWhere('service', 'LIKE', '%' . $q . '%')->orWhere('status', 'LIKE', '%' . $q . '%')->orWhere('group', 'LIKE', '%' . $q . '%')->orWhere('reportedDate', 'LIKE', '%' . $q . '%')->orWhere('note', 'LIKE', '%' . $q . '%')->orWhere('created_at', 'LIKE', '%' . $q . '%')->get();

        return view('result', compact('tasks'));
    }

    public  function viewProccessed($id){
        $data=Task::findOrFail($id);
        return view('viewprocessed',compact('data'));
    }

    public function users(){
        $users=User::all();
        return view('users',compact('users'));
    }

    public  function editUser($id){
        $user=User::find($id);
        return view('edituser',compact('user'));
    }

    public  function userLogout(){
        Auth::logout();
        return redirect('/login')->with('error','Your account is inactive,please contact admin');
    }

    public function test(){
        return view('test');
    }

    public function viewtask($id){
        $tasks=Task::where('estate_id',$id)->get();
        return view('viewtask',compact('tasks'));
    }

    public  function  addestate(Request $request){
        $this->validate($request,[
            'name'=>'required',
            'region'=>'required',
        ]);
        $estate=Estate::create($request->all());
        return redirect()->back()->with('success','Estate Added successfully');
    }
}
