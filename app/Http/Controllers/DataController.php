<?php

namespace App\Http\Controllers;

use App\Estate;
use App\Imports\EstateImport;
use App\Imports\TasksImport;
use App\Jobs\UpdateEstate;
use App\Task;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;

class DataController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }



    public  function submitData(Request $request){
      $this->validate($request,[
       'select_file'=>'required',
      ]);

        Excel::import(new TasksImport(),request()->file('select_file'));

        $job=new UpdateEstate();
        dispatch($job);
        return redirect()->back()->with('success','Your Data has been imported successfully.');

    }

    public  function submitEstate(Request $request){
        $this->validate($request,[
            'select_file'=>'required',
        ]);

        Excel::import(new EstateImport(),request()->file('select_file'));
        return redirect()->back()->with('success','Your estates has been imported successfully.');

    }

    public  function  estateData(){
        $estates=Estate::all();
        return view('estatesData',compact('estates'));
    }

    public function getData(){
        $task=Task::where('status2','Pending')->count();
        $users=User::count();
        $processed=Task::where('status2','Done')->count();
        $estates=Estate::count();
       $data=Array([
           'task'=>$task,
           'users'=>$users,
           'processed'=>$processed,
           'estates'=>$estates,
       ]);
       return $data;
    }

    public  function updateRole(Request $request,$id){
        $user=User::find($id);
        $user->update($request->all());
        return redirect()->route('users')->with('success','Role changed successfully');
    }

    public  function submitProfile(Request $request){
        $this->validate($request,[
            'email'=>'required',
            'name'=>'required',
        ]);
        $user=User::find(auth()->user()->id);
        $user->update($request->all());
//        return ['status'=>true,'message'=>'Your information is updated successfully'];
        return redirect()->back()->with('success','Your information is updated successfully.');
    }

    public  function submitComment(Request $request,$id){
        $this->validate($request,[
            'problemFound'=>'required',
            'actionTaken'=>'required',
        ]);
        $data=Task::findorFail($id);
        $request['user_id']=auth()->user()->id;
        $request['status2']='Done';
        $request['estate_id']='';
        $data->update($request->all());
      return redirect()->route('unprocessed')->with('success','Information submitted successfully');
    }

    public  function submitPassword(Request $request){
        $this->validate($request,[
            'currentpass'=>'required',
            'newpass'=>'required',
        ]);
        $currentpass = auth()->user()->password;
        if (!Hash::check($request['currentpass'], $currentpass)) {
            return ['status'=>false,'message'=>'The current password is wrong.'];
        } elseif ($request->input('newpass') !== $request->input('repass')) {
            return ['status'=>false,'message'=>'The password are not matching'];
        }
        $id = auth()->user()->id;
        $currentUser = User::findOrFail($id);
        $currentUser->password = Hash::make($request->input('newpass'));
        $currentUser->save();
        //return ['status'=>true,'message'=>'Your password has been changed successfully.'];
        return redirect()->back()->with('success','Your password has been changed successfully.');


    }

    public function adduser(Request $request){
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required|unique:users',
            'password'=>'required',
        ]);
       if ($request->input('password') !== $request->input('repass')) {
           return redirect()->back()->with('error','The passwords are not matching');
        }
       $newpass=Hash::make($request->input('password'));
       $request['password']=$newpass;
       $user=User::create($request->all());
       return redirect()->back()->with('success','New user added successfully');
    }

    public  function deleteProcessed($id){
        $data=Task::findorFail($id);
        $data->delete();
        return redirect()->back()->with('success','Data removed successfully');
    }


}
