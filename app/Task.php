<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected  $fillable=['incidentId','lastName','firstName','summery','service','group','reportedDate','targetDate','SLMRealTimeS','duration','resolution','note','problemFound','actionTaken','user_id','status','estate_id','status2'];

    public  function estate(){
        return $this->belongsTo(Estate::class);
    }

    public  function user(){
        return $this->belongsTo(User::class);
    }


}
