<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('incidentId')->nullable();
            $table->string('lastName')->nullable();
            $table->string('firstName')->nullable();
            $table->text('summery')->nullable();
            $table->string('service')->nullable();
            $table->string('group')->nullable();
            $table->string('reportedDate')->nullable();
            $table->string('targetDate')->nullable();
            $table->string('SLMRealTimeS')->nullable();
            $table->string('duration')->nullable();
            $table->text('resolution')->nullable();
            $table->text('note')->nullable();
            $table->string('problemFound')->nullable();
            $table->text('actionTaken')->nullable();
            $table->string('user_id')->nullable();
            $table->string('status')->nullable();
            $table->string('estate_id')->nullable();
            $table->string('status2')->default('Pending')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
