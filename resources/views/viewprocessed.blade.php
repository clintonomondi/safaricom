@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-8">
                @include('includes.message')
                @include('modals.action')
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">{{$data->clientname}} &nbsp;
                        <a class="btn btn-outline-success btn-sm fa fa-check" data-toggle="modal" data-target="#action">Action</a>
                    </div>

                    <div class="card-body">
                        <table class="table table-bordered table-striped" id="myTable">
                            <thead>
                            <th>#</th>
                            <th>#</th>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Incident ID</td>
                                <td>{{$data->incidentId}}</td>
                            </tr>
                            <tr>
                                <td>Last  Name</td>
                                <td>{{$data->lastName}}</td>
                            </tr>
                            <tr>
                                <td>First Name</td>
                                <td>{{$data->firstName}}</td>
                            </tr>
                            <tr>
                                <td>Summery</td>
                                <td>{{$data->summery}}</td>
                            </tr>
                            <tr>
                                <td>Service</td>
                                <td>{{$data->service}}</td>
                            </tr>
                            <tr>
                                <td>Status.</td>
                                <td>{{$data->status}}</td>
                            </tr>
                            <tr>
                                <td>Group</td>
                                <td>{{$data->group}}</td>
                            </tr>
                            <tr>
                                <td>Reported Date</td>
                                <td>{{$data->reportedDate}}</td>
                            </tr>
                            <tr>
                                <td>Note</td>
                                <td>{{$data->note}}</td>
                            </tr>
                            <tr>
                                <td>Problem Found</td>
                                <td>{{$data->problemFound}}</td>
                            </tr>
                            <tr>
                                <td>Action Taken</td>
                                <td>{{$data->actionTaken}}</td>
                            </tr>
                            <tr>
                                <td>Date Solved</td>
                                <td>{{$data->created_at}}</td>
                            </tr>
                            <tr>
                                <td>User</td>
                                <td><a class="" href="{{route('editUser',$data->user->id)}}">{{$data->user->name}}</a> </td>
                            </tr>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
