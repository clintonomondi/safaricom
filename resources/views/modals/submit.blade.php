<!-- The Modal -->
<div class="modal fade" id="myModals2" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

        {{--            <!-- Modal Header -->--}}
        {{--            <div class="modal-header" >--}}
        {{--                <h4 class="modal-title">Please wait...</h4>--}}
        {{--            </div>--}}

        <!-- Modal body -->
            <div class="modal-body">
                <div class="row justify-content-center">
                    <p>Please wait...</p>
                    <div class="col-sm-6">
                        <span class="fa fa-spinner fa-spin fa-3x fa-fw" style="color: #2CB34A;" ></span>
                    </div>
                </div>
                {{--                <div class="progress">--}}
                {{--                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" style="width:100%;"></div>--}}
                {{--                </div>--}}
            </div>

        </div>
    </div>
</div>
