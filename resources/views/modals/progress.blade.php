<!-- The Modal -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <!-- Modal body -->
            <div class="modal-body">
              <div class="row justify-content-center">
                  <p>Please wait...</p>
                  <div class="col-sm-6">
                      <span class="fa fa-spinner fa-spin fa-3x fa-fw" style="color: #2CB34A;" ></span>
                  </div>
              </div>
            </div>

        </div>
    </div>
</div>
