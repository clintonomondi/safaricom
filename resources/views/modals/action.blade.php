<!-- The Modal -->
<div class="modal fade" id="action">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header" style="background-color: #2CB34A;" >
                <h4 class="modal-title">Comment</h4>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form  method="post"  action="{{route('submitComment',$data->id)}}">
                    @csrf
                    <input type="text" name="problemFound" class="form-control" id="form1-username" placeholder="Problem found"><br><br>
                    <div class="form-group">
                        <textarea class="form-control" rows="5" id="comment" name="actionTaken" placeholder="Action taken"></textarea>
                    </div> <br><br>
                    <button type="submit"  class="mb-2 btn btn-success mr-2" style="background-color: #2CB34A;" >SUBMIT</button>
                </form>
            </div>

        </div>
    </div>
</div>
