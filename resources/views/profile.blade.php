@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">User Profile</div>
                @include('includes.message')
                @include('modals.submit')
                <div class="card-body">
<div class="row justify-content-center">
    <div class="col-sm-6">
        <p>Personal Information</p>
        {{--<form id="submitProfile" method="post"  onsubmit="return false">--}}
            <form  method="post"  action="{{route('submitProfile')}}">
            @csrf
            <input type="text" name="name" class="form-control" id="form1-username" placeholder="Name" value="{{Auth::user()->name}}" required><br/>
            <input type="email" name="email" class="form-control" id="form1-username" placeholder="Email" value="{{Auth::user()->email}}" required><br><br>
            <button type="submit"  class="mb-2 btn btn-success mr-2" style="background-color: #2CB34A;">Update</button>
        </form>
    </div>
    <div class="col-sm-6">
<p>Password</p>
        <form  method="post"  action="{{route('submitPassword')}}">
            @csrf
            <input type="password" name="currentpass" class="form-control" id="form1-username" placeholder="Current Password" required><br/>
            <input type="password" name="newpass" class="form-control" id="form1-username" placeholder="New Password" required><br>
            <input type="password" name="repass" class="form-control" id="form1-username" placeholder="Re-enter password"><br><br>
            <button type="submit"  class="mb-2 btn btn-success mr-2" style="background-color: #2CB34A;">Change password</button>
        </form>
    </div>
</div>
                </div>
            </div>
        </div>
    </div>
@endsection
