@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">User Profile</div>
                @include('includes.message')
                @include('modals.submit')
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-sm-6">
                            <p>Change role</p>
                                <input type="text" name="name" class="form-control" id="form1-username" placeholder="Name" value="{{$user->name}}" readonly><br/>
                                <input type="email" name="email" class="form-control" id="form1-username" placeholder="Email" value="{{$user->email}}" readonly><br><br>
<hr>
                            <form  method="post"  action="{{route('updateRole',$user->id)}}">
                                @csrf
                                <div class="form-group">
                                    <select id="inputState" name="role" class="form-control">
                                        <option value="{{$user->role}}">{{$user->role}}</option>
                                        <option value="user">user</option>
                                        <option value="admin">admin</option>
                                        <option value="inactive" style="color: red">InActivate</option>
                                    </select>
                                </div>
                            <button type="submit"  class="mb-2 btn btn-success mr-2" onclick="return submitComment()" style="background-color: #2CB34A;">Update Role</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
