@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">LOGIN</div>
                    <div class="card-body">
                        @include('includes.message')
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                               <input type="email" name="email" class="form-control" id="form1-username" placeholder="Email"><br/>
                               <input type="password" name="password" class="form-control" id="form1-username" placeholder="Password"><br><br>
                               <button type="submit"  class="mb-2 btn btn-success mr-2" style="background-color: #2CB34A;">Login</button><a href="{{route('password.request')}}">Forgot password?</a>
                        </form>
                       </div>
                       </div>
                    </div>
                </div>
            </div>
@endsection
