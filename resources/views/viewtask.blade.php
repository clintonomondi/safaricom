@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">Unprocessed</div>
                @include('includes.message')
                <div class="card-body">
                    <table class="table table-bordered table-striped" id="myTable">
                        <thead>
                        <th>#</th>
                        <th>Client Name</th>
                        <th>Summery</th>
                        <th>Service</th>
                        <th>Status</th>
                        <th></th>
                        <th></th>
                        </thead>
                        <tbody>
                        @foreach($tasks as $key=>$task)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$task->firstName}}</td>
                                <td>{{$task->summery}}</td>
                                <td>{{$task->service}}</td>
                                <td>{{$task->status}}</td>
                                <td><a class="fa fa-check btn btn-success btn-sm" href="{{route('viewUnproccessed',$task->id)}}" style="background-color:#2CB34A;">Process</a> </td>
                                <td>
                                    @can('isAdmin')
                                        <a class="fa fa-trash" href="{{route('deleteData',$task->id)}}" style="color: red;"></a>
                                    @endcan
                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
