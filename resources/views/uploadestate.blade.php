@extends('layouts.app')

@section('content')
    <div class="container">
        @include('includes.message')
        @include('modals.addestate')
        @can('isAdmin')
            <div class="row justify-content-center">
                <div class="col-sm-12">
				<h3>Upload Estates Only</h3>
                    <div class="card">
                        {{--                    <div class="card-header">Upload</div>--}}
                        <div class="card-body">
                            {{--<form method="post" enctype="multipart/form-data" onsubmit="return false" id="submitData">--}}
                            <form method="post" action="{{route('submitEstate')}}" enctype="multipart/form-data">
                                @csrf
                                <div class="row justify-content-center">
                                    <div class="col-sm-4">
                                        <input type="file" name="select_file" class="form-control" id="form1-username" required>
                                    </div>
                                    <div class="col-sm-4">
                                        <button type="submit"  class="mb-2 btn btn-success mr-2">Upload Estates</button>
                                    </div>
                                    <div class="col-sm-4">
                                        <button type="submit"  class="mb-2 btn btn-success mr-2" data-toggle="modal" data-target="#estate">Add estate</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><br>
        @endcan()
        <div class="row">
            <div class="col-lg col-md-6 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                    <div class="card-body p-0 d-flex">
                        <div class="d-flex flex-column m-auto">
                            <div class="stats-small__data text-center">
                                <span class="stats-small__label text-uppercase">Pending</span>
                                <h6 class="stats-small__value count my-3" id="pending"></h6>
                            </div>
                            <div class="stats-small__data">
                                <span class="fa fa-spinner"></span>
                            </div>
                        </div>
                        <canvas height="120" class="blog-overview-stats-small-1"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-lg col-md-6 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                    <div class="card-body p-0 d-flex">
                        <div class="d-flex flex-column m-auto">
                            <div class="stats-small__data text-center">
                                <span class="stats-small__label text-uppercase">Processed</span>
                                <h6 class="stats-small__value count my-3" id="processed"></h6>
                            </div>
                            <div class="stats-small__data">
                                <span class="fa fa-check"></span>
                            </div>
                        </div>
                        <canvas height="120" class="blog-overview-stats-small-2"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-lg col-md-4 col-sm-6 mb-4">
                <div class="stats-small stats-small--1 card card-small">
                    <div class="card-body p-0 d-flex">
                        <div class="d-flex flex-column m-auto">
                            <div class="stats-small__data text-center">
                                <span class="stats-small__label text-uppercase">Estates</span>
                                <h6 class="stats-small__value count my-3" id="estates"></h6>
                            </div>
                            <div class="stats-small__data">
                                <span class="fa fa-building"></span>
                            </div>
                        </div>
                        <canvas height="120" class="blog-overview-stats-small-3"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
