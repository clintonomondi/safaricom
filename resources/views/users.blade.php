@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        @include('includes.message')
                        @include('modals.addusers')
                        <div class="row justify-content-center">
                            <div class="col-sm-6">
                                Users
                            </div>
                            <div class="col-sm-6">
                                <button type="submit" data-toggle="modal" data-target="#adduser"  class="mb-2 btn btn-success mr-2 btn-sm" style="background-clip: #2CB34A;">Add Users</button>
                            </div>
                        </div>

                    </div>

                    <div class="card-body">
                        <table class="table table-bordered table-striped" id="myTable">
                            <thead>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Number of tasks</th>
                            <th>Date added</th>
                            <th></th>
                            </thead>
                            <tbody>
                            @foreach($users as $key=>$user)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    @if(($user->role)=='inactive')
                                        <td style="color: red;">{{$user->role}}</td>
                                    @elseif(($user->role)=='admin')
                                        <td style="color: blue;">{{$user->role}}</td>
                                    @else
                                        <td>{{$user->role}}</td>
                                    @endif
                                    <td></td>
                                    <td>{{$user->created_at}}</td>
                                    <td><a class="fa fa-edit" style="color: #2CB34A;" href="{{route('editUser',$user->id)}}"></a></td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
