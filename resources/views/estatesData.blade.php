@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        @include('includes.message')
                        @include('modals.addusers')
                    </div>

                    <div class="card-body">
                        <table class="table table-bordered table-striped" id="myTable">
                            <thead>
                            <th>#</th>
                            <th>Estate Name</th>
                            <th>Number of Pending Issues</th>
                            <th></th>
                            </thead>
                            <tbody>
                            @foreach($estates as $key=>$estate)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$estate->name}}</td>
                                    <td>{{$estate->task->count()}}</td>
                                    <td><a class="fa fa-eye" style="color: #2CB34A;" href="{{route('viewtask',$estate->id)}}"></a></td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
